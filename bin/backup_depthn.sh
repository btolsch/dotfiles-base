#!/bin/zsh
SELF_DIR=$(dirname $(realpath $0))
source $SELF_DIR/backup_functions.zsh

single_key_prompt() {
  old_stty=$(stty -g)
  stty raw -echo; answer=$(head -c 1); stty $old_stty
  echo $answer
}

HOST=$(hostname)
if [[ $# == 2 ]]; then
  HOST=$2
fi

echo -n "propagate to depth $1 for host $HOST? [y/N] "
answer=$(single_key_prompt); echo
if [[ "$answer" != "y" && "$answer" != "Y" ]]; then
  exit
fi

backup_depthn_base $HOST $1
