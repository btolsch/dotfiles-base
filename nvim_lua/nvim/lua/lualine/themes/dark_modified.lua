return {
  normal = {
    a = {fg = "#000000", bg = "#acec00"},
    b = {fg = "#b5b5b5", bg = "#444444"},
    c = function(section)
      if vim.bo.modified then
        return {fg = "#ffffff", bg = "#5f005f"}
      else
        return {fg = "#b2d631", bg = "#202020"}
      end
    end,
  },
  insert = {
    a = function(section)
      if vim.go.paste then
        return {fg = "#000000", bg = "#d78700"}
      else
        return {fg = "#000000", bg = "#0a76d7"}
      end
    end,
    b = {fg = "#000000", bg = "#0650cf"},
    c = function(section)
      if vim.bo.modified then
        return {fg = "#b5b5b5", bg = "#5f005f"}
      else
        return {fg = "#000000", bg = "#054090"} 
      end
    end,
  },
  visual = {
    a = {fg = "#000000", bg = "#ffaf00"},
    b = {fg = "#000000", bg = "#ff8800"},
    c = function(section)
      if vim.bo.modified then
        return {fg = "#b5b5b5", bg = "#5f005f"}
      else
        return {fg = "#000000", bg = "#ff5f00"}
      end
    end,
  },
  replace = {
    a = {fg = "#000000", bg = "#d70f0f"},
    b = {fg = "#000000", bg = "#ba1717"},
    c = function(section)
      if vim.bo.modified then
        return {fg = "#b5b5b5", bg = "#5f005f"}
      else
        return {fg = "#000000", bg = "#910808"}
      end
    end,
  },
  command = {
    a = {fg = "#000000", bg = "#acec00"},
    b = {fg = "#000000", bg = "#444444"},
    c = {fg = "#000000", bg = "#202020"},
  },
  inactive = {
    a = {fg = "#4e4e4e", bg = "#1c1c1c"},
    b = {fg = "#4e4e4e", bg = "#262626"},
    c = function(section)
      if vim.bo.modified then
        return {fg = "#875faf", bg = ""}
      else
        return {fg = "#8e8e8e", bg = "#2c2c2c"}
      end
    end,
  },
}
