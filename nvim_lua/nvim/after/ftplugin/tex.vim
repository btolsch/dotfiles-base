setlocal conceallevel=1
hi Conceal ctermbg=None guibg=None
" TODO(btolsch): This setting doesn't seem to work.  Maybe vimtex is controlling conceal?
let g:tex_conceal = 'abdmg'
" let g:vimtex_delim_toggle_mod_list =
