local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

vim.g.mapleader = ","
vim.g.maplocalleader = ","

-- TODO(btolsch): It seems there are many plugin globals that need to be set before loading, which
-- is now here instead after the entire file runs.
vim.g.airline_powerline_fonts = 1
vim.cmd([[
  let g:airline#extensions#tabline#enabled = 1
  let g:airline#extensions#tabline#show_buffers = 1
  let g:airline#extensions#tabline#fnamemod = ':t'
  let g:airline_detect_modified = 1
  let g:airline_theme = 'dark_modified'
]])

vim.g.lf_map_keys = 0
vim.g.lf_replace_netrw = 1
vim.g.lf_width = 0.95
vim.g.lf_height = 0.95

require("lazy").setup({
  "hrsh7th/cmp-nvim-lsp",
  "hrsh7th/cmp-buffer",
  "hrsh7th/cmp-path",
  "hrsh7th/cmp-cmdline",
  "hrsh7th/nvim-cmp",
  {
    "SirVer/ultisnips",
    init = function(args)
      vim.cmd([[
        let g:UltiSnipsExpandTrigger = '<c-space>'
      ]])
    end,
  },
  "quangnguyen30192/cmp-nvim-ultisnips",
  --"honza/vim-snippets",
  -- "dcampos/cmp-snippy",
  -- "dcampos/nvim-snippy",
  "djoshea/vim-autoread",
--  {"nvim-lualine/lualine.nvim", dependencies = { "nvim-tree/nvim-web-devicons" } },
--  "kdheepak/tabline.nvim",
  -- {dir = "~/code/tabline.nvim"},
  "vim-airline/vim-airline",
--  "vim-airline/vim-airline-themes",
  "tpope/vim-fugitive",
  "christoomey/vim-tmux-navigator",
  "benmills/vimux",
  "airblade/vim-gitgutter",
  "tmux-plugins/vim-tmux-focus-events",
  "tpope/vim-surround",
-- Plug 'b4winckler/vim-angry'
  "tomtom/tcomment_vim",
  "ptzz/lf.vim",
  "voldikss/vim-floaterm",
  "tikhomirov/vim-glsl",
  "easymotion/vim-easymotion",
  "tpope/vim-repeat",
  "haya14busa/incsearch.vim",
  "haya14busa/incsearch-easymotion.vim",
  "haya14busa/incsearch-fuzzy.vim",
  {
    "ibhagwan/fzf-lua",
    config = function()
      require("plugins.fzf")
    end
  },
  "plasticboy/vim-markdown",
  "jkramer/vim-checkbox",
  "ziglang/zig.vim",
  "sk1418/HowMuch",
  "moll/vim-bbye",
  {"nvim-treesitter/nvim-treesitter", build = ":TSUpdate"},
  "nvim-treesitter/nvim-treesitter-context",
  --{dir="~/code/nvim-treesitter-context"},
  "nvim-treesitter/nvim-treesitter-textobjects",
  {dir = "/usr/share/vim/vimfiles"},
  "ray-x/lsp_signature.nvim",
  {
    "lervag/vimtex",
    init = function()
      vim.cmd([[
        let g:vimtex_view_method = 'zathura'
      ]])
    end,
  },
  -- stevearc/aerial.nvim also an option.
  {
    "hedyhli/outline.nvim",
    config = function()
      vim.keymap.set("n", "<leader>ot", "<cmd>Outline<CR>", { desc = "Toggle Outline" })
      require("outline").setup()
    end,
  },
  {
    "arnamak/stay-centered.nvim",
    config = function()
      require("stay-centered").setup({
        skip_filetypes = { "Outline" },
        enable = true,
      })
      vim.keymap.set({ 'n', 'v' }, '<leader>sc', require('stay-centered').toggle, { desc = 'Toggle stay-centered.nvim' })
    end,
  },
  {
    "chrisgrieser/nvim-spider",
    lazy = true,
    init = function()
      vim.keymap.set({ "n", "o", "x" }, "w", function()
        require('spider').motion('w')
      end, { desc = "Spider-w" })
      vim.keymap.set({ "n", "o", "x" }, "e", function()
        require('spider').motion('e')
      end, { desc = "Spider-e" })
      vim.keymap.set({ "n", "o", "x" }, "b", function()
        require('spider').motion('b')
      end, { desc = "Spider-b" })
    end
  },
  {
    "sindrets/diffview.nvim",
    config = function()
      require("plugins.diffview")
    end,
  },
})

vim.api.nvim_create_autocmd("LspAttach", {
  callback = function(args)
    vim.bo[args.buf].formatexpr = nil

    vim.keymap.set('n', 'K', function() vim.lsp.buf.hover() end)
    vim.keymap.set("n", "[d", function() vim.lsp.buf.definition() end)
    vim.keymap.set('n', '[t', function() vim.lsp.buf.type_definition() end)
    vim.keymap.set('n', '<leader>r', function() vim.lsp.buf.references() end)
    vim.keymap.set('n', '<leader>sh', function() vim.lsp.buf.signature_help() end)
    vim.keymap.set('n', '[a', function()
      vim.diagnostic.goto_prev()
      vim.cmd [[normal! zz]]
    end)
    vim.keymap.set('n', ']a', function()
      vim.diagnostic.goto_next()
      vim.cmd [[normal! zz]]
    end)
    vim.keymap.set('n', 'xr', function() vim.lsp.buf.rename() end)
    vim.keymap.set('n', '<space>e', function() vim.diagnostic.open_float() end)
    vim.keymap.set('n', '<space>d',
      function()
        if vim.diagnostic.is_disabled(0) then
          vim.diagnostic.enable(0)
        else
          vim.diagnostic.disable(0)
        end
      end
    )
    vim.api.nvim_create_autocmd(
      {"CursorHold", "CursorHoldI"},
      {callback = function(args) vim.lsp.buf.document_highlight() end}
    )
    vim.api.nvim_create_autocmd(
      "CursorMoved",
      {callback = function(args) vim.lsp.buf.clear_references() end}
    )
  end
})
vim.api.nvim_create_user_command(
    "LspStop", function(args) vim.lsp.stop_client(vim.lsp.get_active_clients()) end, {})

require("lsp_signature").setup(
    {
      max_width = 200,
      hint_prefix = "» ",
      -- TODO(btolsch): Different toggle key?  I tried to <c-e> in visual mode and later didn't
      -- realize that disabled the signature help popup.
      toggle_key = "<c-e>",
      toggle_key_flip_floatwin_setting = true,
      timer_interval = 50,
      floating_window_off_y = -5,
    }, 0)

local t = function(str)
  return vim.api.nvim_replace_termcodes(str, true, false, true)
end

local cmp = require("cmp")
cmp.setup({
  snippet = {
    -- REQUIRED - you must specify a snippet engine
    expand = function(args)
      -- vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
      -- luasnip.lsp_expand(args.body) -- For `luasnip` users.
      -- snippy.expand_snippet(args.body) -- For `snippy` users.
      vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
    end,
  },
  window = {
    -- completion = cmp.config.window.bordered(),
    -- documentation = cmp.config.window.bordered(),
  },
  mapping = {
    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        if #cmp.get_entries() == 1 then
          cmp.confirm({ select = true })
        else
          cmp.select_next_item()
        end
      elseif vim.fn["UltiSnips#CanJumpForwards"]() == 1 then
        if vim.api.nvim_get_mode()["mode"] == "i" then
          vim.api.nvim_feedkeys(t("<c-r>=UltiSnips#JumpForwards()<cr>"), 'n', true)
        else
          vim.api.nvim_feedkeys(t("<esc>:call UltiSnips#JumpForwards()<cr>"), 'n', true)
        end
      else
        fallback()
      end
    end, { "i", "s" }),

    ["<S-Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif vim.fn["UltiSnips#CanJumpBackwards"]() == 1 then
        if vim.api.nvim_get_mode()["mode"] == "i" then
          vim.api.nvim_feedkeys(t("<c-r>=UltiSnips#JumpBackwards()<cr>"), 'n', true)
        else
          vim.api.nvim_feedkeys(t("<esc>:call UltiSnips#JumpBackwards()<cr>"), 'n', true)
        end
      else
        fallback()
      end
    end, { "i", "s" }),
    ["<c-n>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        if #cmp.get_entries() == 1 then
          cmp.confirm({ select = true })
        else
          cmp.select_next_item()
        end
      elseif vim.fn["UltiSnips#CanJumpForwards"]() == 1 then
        if vim.api.nvim_get_mode()["mode"] == "i" then
          vim.api.nvim_feedkeys(t("<c-r>=UltiSnips#JumpForwards()<cr>"), 'n', true)
        else
          vim.api.nvim_feedkeys(t("<esc>:call UltiSnips#JumpForwards()<cr>"), 'n', true)
        end
      else
        fallback()
      end
    end, { "i", "s" }),

    ["<c-p>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif vim.fn["UltiSnips#CanJumpBackwards"]() == 1 then
        if vim.api.nvim_get_mode()["mode"] == "i" then
          vim.api.nvim_feedkeys(t("<c-r>=UltiSnips#JumpBackwards()<cr>"), 'n', true)
        else
          vim.api.nvim_feedkeys(t("<esc>:call UltiSnips#JumpBackwards()<cr>"), 'n', true)
        end
      else
        fallback()
      end
    end, { "i", "s" }),
    ['<C-b>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.confirm({ select = true }),
    ['<C-e>'] = cmp.mapping.abort(),
    ['<CR>'] = function(fallback)
      if cmp.get_selected_entry() ~= nil then
        cmp.confirm({ select = true })
        vim.api.nvim_feedkeys('\n', 'n', false)
      else
        fallback()
      end
    end,
  },
  sources = {
    { name = 'nvim_lsp' },
    -- { name = 'vsnip' }, -- For vsnip users.
    -- { name = 'luasnip' }, -- For luasnip users.
    { name = 'ultisnips' }, -- For ultisnips users.
    -- { name = 'snippy' }, -- For snippy users.
    {
      name = 'buffer',
      option = {
        get_bufnrs = function() return vim.api.nvim_list_bufs() end,
      },
    },
  },
})

-- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline({ '/', '?' }, {
  mapping = cmp.mapping.preset.cmdline(),
  sources = {
    { name = 'buffer' }
  }
})

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(':', {
  mapping = cmp.mapping.preset.cmdline(),
  sources = cmp.config.sources({
    { name = 'path' }
  }, {
    { name = 'cmdline' }
  })
})

require("treesitter-context").setup({
  enable = true, -- Enable this plugin (Can be enabled/disabled later via commands)
  max_lines = -1, -- How many lines the window should span. Values <= 0 mean no limit.
  min_window_height = 0, -- Minimum editor window height to enable context. Values <= 0 mean no limit.
  line_numbers = true,
  multiline_threshold = 20, -- Maximum number of lines to show for a single context
  trim_scope = 'outer', -- Which context lines to discard if `max_lines` is exceeded. Choices: 'inner', 'outer'
  mode = 'cursor',  -- Line used to calculate context. Choices: 'cursor', 'topline'
  -- Separator between context and content. Should be a single character string, like '-'.
  -- When separator is set, the context will only show up when there are at least 2 lines above cursorline.
  separator = nil,
  zindex = 20, -- The Z-index of the context window
  on_attach = nil, -- (fun(buf: integer): boolean) return false to disable attaching
})

local ts_configs = require("nvim-treesitter.configs")
local ts_parsers = require("nvim-treesitter.parsers")

ts_configs.setup({
  -- A list of parser names, or "all" (the five listed parsers should always be installed)
  ensure_installed = { "c", "lua", "vim", "vimdoc", "query" },

  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- Automatically install missing parsers when entering buffer
  -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
  auto_install = false,

  -- List of parsers to ignore installing (or "all")
  -- ignore_install = { "javascript" },

  ---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
  -- parser_install_dir = "/some/path/to/store/parsers", -- Remember to run vim.opt.runtimepath:append("/some/path/to/store/parsers")!

  highlight = { enable = false },
  indent = { enable = false },
  textobjects = {
    select = {
      enable = true,

      -- Automatically jump forward to textobj, similar to targets.vim
      lookahead = true,

      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ["af"] = "@function.outer",
        ["if"] = "@function.inner",
        ["aa"] = "@parameter.outer",
        ["ia"] = "@parameter.inner",
        -- You can also use captures from other query groups like `locals.scm`
        ["as"] = { query = "@scope", query_group = "locals", desc = "Select language scope" },
      },
      -- You can choose the select mode (default is charwise 'v')
      --
      -- Can also be a function which gets passed a table with the keys
      -- * query_string: eg '@function.inner'
      -- * method: eg 'v' or 'o'
      -- and should return the mode ('v', 'V', or '<c-v>') or a table
      -- mapping query_strings to modes.
      selection_modes = {
        ['@parameter.outer'] = 'v', -- charwise
        ['@function.outer'] = 'V', -- linewise
        ['@class.outer'] = '<c-v>', -- blockwise
      },
      -- If you set this to `true` (default is `false`) then any textobject is
      -- extended to include preceding or succeeding whitespace. Succeeding
      -- whitespace has priority in order to act similarly to eg the built-in
      -- `ap`.
      --
      -- Can also be a function which gets passed a table with the keys
      -- * query_string: eg '@function.inner'
      -- * selection_mode: eg 'v'
      -- and should return true of false
      include_surrounding_whitespace = function(args)
        if args["query_string"] == "@parameter.outer" or args["query_string"] == "@function.outer" then
          return true
        end
        return false
      end,
    },
    swap = {
      enable = true,
      swap_next = {
        ["[]"] = "@parameter.inner",
      },
      swap_previous = {
        ["]["] = "@parameter.inner",
      },
    },
    move = {
      enable = true,
      disable = { "markdown" },
      set_jumps = false, -- whether to set jumps in the jumplist
      goto_next_start = {
        ["]m"] = "@parameter.inner",
        ["]s"] = { query = "@scope", query_group = "locals", desc = "Next scope" },
        ["]f"] = "@function.outer",
      },
      goto_next_end = {
        ["}M"] = "@parameter.inner",
        ["}S"] = { query = "@scope", query_group = "locals", desc = "Next scope" },
      },
      goto_previous_start = {
        ["[m"] = "@parameter.inner",
        ["[s"] = { query = "@scope", query_group = "locals", desc = "Next scope" },
        ["[f"] = "@function.outer",
      },
      goto_previous_end = {
        ["{M"] = "@parameter.inner",
        ["{S"] = { query = "@scope", query_group = "locals", desc = "Next scope" },
      },
    },
  },
})

-- Make sure TS syntax tree is updated when needed by plugin (with some throttling)
-- even if the `highlight` module is not enabled.
-- See https://github.com/nvim-treesitter/nvim-treesitter/issues/2492
_G.TreesitterParse = function()
  local lang = ts_parsers.ft_to_lang(vim.bo.filetype)
  local parser = ts_parsers.get_parser(vim.fn.bufnr(), lang)
  if parser then
    return parser:parse()
  else
    return false
  end
end
local function throttle(fn, ms)
  local timer = vim.loop.new_timer()
  local running = false
  return function(...)
    if not running then
      timer:start(ms, 0, function() running = false end)
      running = true
      pcall(vim.schedule_wrap(fn), select(1, ...))
    end
  end
end
if not ts_configs.get_module('highlight').enable then
  _G.TreesitterParseDebounce = throttle(_G.TreesitterParse, 100)  -- 100 ms
  vim.cmd [[
    augroup TreesitterUpdateParsing
      autocmd!
      autocmd TextChanged,TextChangedI *   call v:lua.TreesitterParseDebounce()
    augroup END
  ]]
end

vim.keymap.set({'n', 'v'}, "[u", function() require("treesitter-context").go_to_context(vim.v.count1) end, { silent = true })
vim.keymap.set({'n', 'v'}, "<space>fj", "]fzz", {remap = true})
vim.keymap.set({'n', 'v'}, "<space>fk", "[fzz", {remap = true})

vim.keymap.set({'n', 'v'}, ';', ':')
vim.keymap.set({'n', 'v'}, ':', ';')

vim.opt.termguicolors = true
-- NOTE(btolsch): I got airline working again, but keeping this in case I ever get a better lua
-- bufferline solution, since I like lua customization more.
-- require("lualine").setup({
--   options = {
--     theme = "dark_modified",
--     colored = false,
--   },
--   sections = {
--     lualine_y = {},
--     lualine_z = {
--       function()
--         local cur = vim.fn.line('.')
--         local total = vim.fn.line('$')
--         -- TODO(btolsch): Can we get both ends of a visual selection to display the range?
--         local result = ' :' .. cur .. '/' .. total .. '»' .. string.format("%3d", vim.fn.virtcol('.'))
--         if cur == 1 then
--           return 'Top' .. result
--         elseif cur == total then
--           return 'Bot' .. result
--         else
--           return string.format('%2d%%%%', math.floor(cur / total * 100)) .. result
--         end
--       end
--     },
--   },
-- })
-- require("tabline").setup({
--   enable = true,
--   options = {
--     show_devicons = false,
--   },
-- })

-- lf.vim doesn't properly pass open command anymore.
vim.g.floaterm_opener = "edit"

-- TODO(btolsch): I'm not sure it's worth porting over all the misc settings and autocmds.
vim.opt.cursorline = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.laststatus = 2
vim.opt.showmode = false

vim.g.c_syntax_for_h = 1

vim.cmd("runtime oldinit.vim")
