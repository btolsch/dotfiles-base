#!/bin/zsh

# TODO(btolsch): Remove this from other platform install scripts.
single_key_prompt() {
  old_stty=$(stty -g)
  stty raw -echo; answer=$(head -c 1); stty $old_stty
  echo $answer
}

get_rel_path() {
  # TODO(btolsch): This was previously sans-'-s', then got '-s', but now I think
  # it should be removed again.  Here is an example that seems to be
  # problematic for '-s' (and no other cases seem to be broken by removing
  # it).
  #
  #   dotfiles checked out to ~/dotfiles
  #   ln -s ~/.config/nvim ../../dotfiles/base/.vim
  #   cd ~/dotfiles/base
  #   realpath -s ../.vimrc --relative-to=$(dirname ~/.config/nvim/init.vim)
  #
  #   output should be ../../.vimrc, but is ../../dotfiles/.vimrc
  #   without -s it is correct.
  realpath $1 --relative-to=$(dirname $2)
}

symlink_install() {
  dest_file=$1
  file=$2
  override=$3

  rel_file=$(get_rel_path $file $dest_file)
  if [ ! -e $dest_file -a ! -L $dest_file ]; then
    mkdir -p $(dirname $dest_file)
    ln -s $rel_file $dest_file
  # resolves
  elif [ ! -L $dest_file ]; then
    echo "$dest_file exists and is not a link"
    if [ -n "$override" -a "$override" != 0 ]; then
      echo "overwriting $dest_file with link ($rel_file)"
      ln -snf $rel_file $dest_file
    fi
  # !resolves && paths differ
  elif [ "$(readlink -f $dest_file)" != "$(realpath $file)" ]; then
    echo "$dest_file exists, and is a link, but points somewhere else ($(readlink -f $dest_file) vs. $(realpath $file) ($rel_file))"
    if [ -n "$override" -a "$override" != 0 ]; then
      echo "overwriting $dest_file with different link ($rel_file)"
      ln -snf $rel_file $dest_file
    fi
  else
    echo "okay: $dest_file"
  fi
}
