#!/bin/zsh

SELF_DIR=$(dirname $(realpath $0))
pushd $SELF_DIR/fonts

LINK_PREFIX=$(realpath --relative-to=$HOME/.local/share/fonts $SELF_DIR/fonts)

for f in *; do
  if [[ -d "$f" && "$f" != "fontconfig" ]]; then
    ln -sf "$LINK_PREFIX/$f" "$HOME/.local/share/fonts/$f"
  fi
done
popd

fc-cache -f
