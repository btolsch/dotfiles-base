#!/bin/zsh
#override existing non-link regular files with first argument ($1)
#sub-scripts get passed second argument ($2) as their arg

SELF_DIR=$(dirname $(realpath $0))
pushd $SELF_DIR

PLATFORM_REL_PREFIX=$(realpath --relative-to=$SELF_DIR $HOME/dotfiles)
OVERRIDE=${1:-0}

source install_functions.zsh

batch_symlink_install() {
  batch_dict_symlink_install $(paste -d ' ' <(echo $@ | tr ' ' '\n') <(echo $@ | tr ' ' '\n') | tr '\n' ' ')
}

batch_dict_symlink_install() {
  local -A files
  files=($@)
  for dest_file in ${(k)files}; do
    if [ "${platform_symlink_files[$dest_file]}" = "" ]; then
      file=${files[$dest_file]}
      symlink_install ~/$dest_file $file $OVERRIDE
    fi
  done
}

if ! which st &>/dev/null; then
  echo -n "try to install st? [Y/n] "
  answer=$(single_key_prompt)
  echo
  if [ "$answer" != "n" -a "$answer" != "N" ]; then
    mkdir -p ~/code
    mkdir -p ~/.local/bin
    pushd ~/code
    git clone https://git.suckless.org/st
    cd st/
    git checkout 0.8.2 && \
    patch -Np1 <$SELF_DIR/st/st-alpha-0.8.2.diff && \
    cp config.def.h config.h && \
    patch -Np1 <$SELF_DIR/st/config.h-0.8.2.diff && \
    make && \
    cp st ~/.local/bin
    popd
  fi
fi

if ! git remote -v | grep -q 'git@gitlab.com'; then
  echo -n "add ssh git remote? [Y/n] "
  answer=$(single_key_prompt)
  echo
  if [[ "$answer" != "n" && "$answer" != "N" ]]; then
    git remote add origin-ssh 'git@gitlab.com:btolsch/dotfiles-base.git'
  fi
fi

typeset -A other_files
other_files=(
    ".config/nvim" "nvim_lua/nvim"
    ".config/mpd/mpd.conf" "mpd.conf"
    ".config/picom.conf" "picom.conf"
    ".config/powerline" "powerline"
    ".config/systemd/user/redshift-gtk.service" "redshift-gtk.service"
    ".config/systemd/user/redshift-gtk.service.d" "redshift-gtk.service.d"
    ".config/redshift/redshift.conf" "redshift.conf"
    ".config/sxhkd/sxhkdrc" "sxhkd/normal"
    ".config/alacritty/alacritty.yml" "alacritty.yml"
    ".config/lf/lfrc" "lfrc"
    ".config/lf/preview.sh" "preview.sh"
    ".config/rofi/config.rasi" "config.rasi"
)
if [[ ${(k)platform_symlink_files} != ${(k)platform_symlink_prefixes} ]]; then
  echo "keys !="
  echo FILES
  for k in ${(k)platform_symlink_files}; do
    echo $k: ${platform_symlink_files[$k]}
  done
  echo PREFIXES
  for k in ${(k)platform_symlink_prefixes}; do
    echo $k: ${platform_symlink_prefixes[$k]}
  done
  exit 1
fi
batch_symlink_install $(ls -d .* | grep -v ".git")
batch_symlink_install bin/*
batch_dict_symlink_install ${(kv)other_files}
for dest_file in ${(k)platform_symlink_files}; do
  file=${platform_symlink_files[$dest_file]}
  symlink_install ~/$dest_file $PLATFORM_REL_PREFIX/${platform_symlink_prefixes[$dest_file]}/$file $1
done

for script in $(ls | grep -E ".install\.sh"); do
  echo "running $script"
  source $script $2
done

popd
