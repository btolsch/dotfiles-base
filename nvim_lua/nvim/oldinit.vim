autocmd! bufwritepost .vimrc source %

let clang_format_py = '/usr/share/clang/clang-format.py'
let g:powerline_pycmd = 'py3'

nnoremap <silent> n nzz
nnoremap <silent> N Nzz
nnoremap <c-d> M<c-d>zz
nnoremap <c-u> M<c-u>zz
nnoremap <c-f> M<c-f>zz
nnoremap <c-b> M<c-b>zz

nnoremap <silent> <space>x :silent !chmod +x %<cr>

nnoremap <silent> <leader>bd :Bdelete<cr>

nnoremap <leader>as :%s/\<<c-r><c-w>\>/<c-r><c-w>/gI<left><left><left>

set matchpairs+=<:>

set termguicolors
set guicursor=a:blinkon0
filetype plugin indent on
syntax on
colorscheme BusyBee_modified
" colorscheme darcula
" colorscheme nord
" colorscheme gruvbox
hi Normal guibg=NONE
set modelines=0

" Leader keybindings
"
" /     :nohl
" bd    delete current buffer but don't close window
" bs    bufdo vimgrepadd @pattern@g | cw
" cc    cclose
" cf    clang format
" co    copen
" cw    cw
" d     "_d
" D     "_D
" e     edit %:h
" gd    :Gdiff
" gdp   :Gdiff for some commit
" gs    :Gstatus
" h     tab previous
" j     tab first
" k     tab last
" l     tab next
" m     max height
" c-m   clear, redraw, make
" n     max width
" c-n   redraw
" o     only
" pc    close preview window
" r     edit .
" s     sort lines
" si    echo syntaxId
" so    source .vimrc
" tl    c-p and enter in runner
" v     select recently pasted text
" vc    vimux interrupt runner
" vl    vimux run last command
" vm    vimux run make
" vo    vimux open runner select
" vp    vimux prompt command
" vq    vimux close runner
" vz    vimux zoom runner
" w     open vertical split window
" wb    <c-w>b
" wp    <c-w>p
" ws    delete trailing whitespace

nnoremap <leader>w :w<cr>
nnoremap <leader>s <c-w>v<c-w>l
nnoremap <leader>v V']
nnoremap <leader>wb <c-w>b
nnoremap <leader>wp <c-w>p
nnoremap <leader>pc :pc<cr>
nnoremap <leader>e :Lf<cr>
nnoremap <leader>le :lopen<cr>
nnoremap <leader>lc :lclose<cr>
nnoremap <silent> <leader>p :set paste!<cr>

om <leader>a aa
vm <leader>a aa

let g:HowMuch_no_mappings = 1
vmap <a-a> <Plug>AutoCalcAppendWithEq
vmap <a-s> <Plug>AutoCalcReplace

" autocmd BufEnter * call ncm2#enable_for_buffer()
set completeopt=noinsert,menuone,noselect

autocmd InsertLeave,CompleteDone * if pumvisible() == 0 && &buftype == '' | pclose | endif

set shortmess+=c

let g:gitgutter_max_signs = 2000

" incsearch.vim x fuzzy x vim-easymotion
function! s:config_easyfuzzymotion(...) abort
  return extend(copy({
  \   'converters': [incsearch#config#fuzzy#converter()],
  \   'modules': [incsearch#config#easymotion#module()],
  \   'keymap': {"\<CR>": '<Over>(easymotion)'},
  \   'is_expr': 0,
  \   'is_stay': 1
  \ }), get(a:, 1, {}))
endfunction

noremap <silent><expr> <Space>/ incsearch#go(<SID>config_easyfuzzymotion())

" nmap <leader>;         <Plug>(easymotion-repeat)
" map  <leader><leader>j <Plug>(easymotion-sol-bd-jk)
" map  <leader>j         <Plug>(easymotion-bd-jk)
" map          /         <Plug>(easymotion-sn)
" omap         /         <Plug>(easymotion-tn)
" map  <leader>f         <Plug>(easymotion-bd-f2)
" nmap <leader>f         <Plug>(easymotion-overwin-f2)
" map  <leader>t         <Plug>(easymotion-bd-t)
nmap          f         <Plug>(easymotion-bd-fl)
xmap          f         <Plug>(easymotion-bd-fl)
omap          f         <Plug>(easymotion-bd-fl)
nmap          t         <Plug>(easymotion-bd-tl)
xmap          t         <Plug>(easymotion-bd-tl)
omap          t         <Plug>(easymotion-bd-tl)
" map  <leader>h         <Plug>(easymotion-lineanywhere)
" map  <leader>l         <Plug>(easymotion-lineanywhere)
" map  <leader>w         <Plug>(easymotion-bd-w)
" nmap <leader>w         <Plug>(easymotion-overwin-w)

let g:EasyMotion_use_upper = 1
let g:EasyMotion_keys = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ;'
let g:EasyMotion_smartcase = 1
let g:EasyMotion_use_smartsign_us = 1
let g:EasyMotion_startofline = 0

nn <silent> JK :exe "e " . expand("<cWORD>")<cr>

" Copynpaste out of vim-tmux-navigator.
function! s:TmuxSocket()
  " The socket path is the first value in the comma-separated list of $TMUX.
  return split($TMUX, ',')[0]
endfunction
function! s:TmuxCommand(args)
  let cmd = 'tmux -S ' . s:TmuxSocket() . ' ' . a:args
  let l:x=&shellcmdflag
  let &shellcmdflag='-c'
  let retval=system(cmd)
  let &shellcmdflag=l:x
  return retval
endfunction

if empty($TMUX)
  nn <silent> <leader>b :let @+ = 'b ' . expand('%:p') . ':' . line('.')<cr>
else
  nn <silent> <leader>b :call <SID>TmuxCommand('set-buffer "b ' . expand('%:p') . ':' . line('.') . '"')<cr>
endif
autocmd TextYankPost * silent! if v:event["regname"] == "+" | call system("tmux loadb -", join(v:event["regcontents"], "\n")) | endif

let g:python_host_prog = '/usr/bin/python2'
let g:python3_host_prog = '/usr/bin/python3'

let g:vim_markdown_no_default_key_mappings = 1
let g:vim_markdown_new_list_item_indent = 2

set previewheight=25
set timeoutlen=250 ttimeoutlen=0

set omnifunc=syntaxcomplete#Complete

nnoremap <leader>bs :cex []<bar>execute 'bufdo vimgrepadd @@g %'<bar>cw<s-left><s-left><right>

hi link cDefine PreProc
hi link cInclude PreProc
hi link cPrecondit PreProc

hi link pythonInclude PreProc

hi! link FoldColumn LineNr

hi link qfFileName PreProc
hi link qfLineNr Number
hi link QuickFixLine CursorLine

hi! DiagnosticError guifg=#d34252
hi! DiagnosticWarn guifg=#b38d62
hi! htmlLink gui=underline guifg=#628db3

hi! TreesitterContext guibg=#484848

" hi! link Conceal Normal

set updatetime=1000
hi LspReferenceText guibg=#3e4a82 ctermbg=103
hi LspReferenceRead guibg=#3e4a82 ctermbg=103
hi LspReferenceWrite guibg=#3e4a82 ctermbg=103

hi FloatBorder guifg=#628db3 guibg=#202020

autocmd FocusLost,WinLeave,InsertEnter * set norelativenumber number
autocmd FocusGained,WinEnter,InsertLeave * set relativenumber number

set fillchars+=vert:\ " comment

nnoremap <leader>si :call SyntaxItem()<CR>
function! SyntaxItem()
  echo synIDattr(synID(line("."), col("."), 1), "name")
endfunction

nnoremap j gj
nnoremap k gk

set hidden
set confirm
set splitbelow
set splitright

set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set cindent
set cino=N-s,g0,(0,W2s,j1 " ,+2s

" autocmd FileType markdown call ncm2#disable_for_buffer()
" autocmd FileType text call ncm2#disable_for_buffer()
" autocmd FileType gitcommit call ncm2#disable_for_buffer()
autocmd FileType text setlocal nocindent autoindent fo=t
autocmd FileType gitcommit setlocal tw=72 nocindent autoindent fo=t
autocmd FileType make setlocal noexpandtab
autocmd FileType vim setlocal fdc=1
autocmd FileType vim setlocal foldlevel=0
autocmd FileType vim setlocal foldmethod=marker
autocmd BufNewFile,BufRead /tmp/mutt* setlocal autoindent nocindent filetype=mail tw=80 digraph
autocmd BufNewFile,BufRead ~/tmp/mutt* setlocal autoindent nocindent filetype=mail tw=80 digraph
autocmd BufNewFile,BufRead *.vs,*.gs,*.fs set filetype=glsl

nn <silent> <leader>so :source $MYVIMRC<cr>

nnoremap <silent> <leader>cf :call FormatAll()<CR>
execute 'vnoremap <silent> <leader>cf :py3f ' . clang_format_py . '<CR>'

let s:clang_format_py = clang_format_py
function! FormatAll()
  let l:lines="all"
  execute 'py3f ' . s:clang_format_py
endfunction

nnoremap <silent> <leader>gf :pyf $HOME/.vim/plugged/gn/misc/vim/gn-format.py<cr>

call tcomment#type#Define('c', tcomment#GetLineC('// %s'))

set makeprg=./build.sh
set errorformat=%f:%l:%c:\ error:\ %m,%f:%l:%c:\ fatal\ error:\ %m
nnoremap <silent> <leader>bl :make<cr>
nnoremap <silent> <leader>z :make<cr>

inoremap <c-c> TODO(btolsch):<space>
inoremap <c-b> NOTE(btolsch):<space>

nnoremap <silent> <M-h> :bprevious<CR>
nnoremap <silent> <M-l> :bnext<CR>
nnoremap <silent> <M-j> :tabprevious<CR>
nnoremap <silent> <M-k> :tabnext<CR>
nnoremap <silent> <M-;> :b#<cr>
nnoremap <silent> [q :cprev<cr>zz
nnoremap <silent> ]q :cnext<cr>zz
nnoremap <silent> <leader>co :copen<cr>
nnoremap <silent> <leader>cc :cclose<cr>
nnoremap <silent> <leader>cw :cw<cr>

nmap [c <Plug>(GitGutterPrevHunk)zz
nmap ]c <Plug>(GitGutterNextHunk)zz

nnoremap <silent> <leader>m <C-w>_
nnoremap <silent> <leader>n <C-w>\|

nnoremap <silent> <M-<> :tabm -1<CR>
nnoremap <silent> <M->> :tabm +1<CR>
nnoremap <C-M-n> :tab split<cr>

" Insert home key motion with Alt
inoremap <M-h> <C-o>h
inoremap <M-j> <C-o>j
inoremap <M-k> <C-o>k
inoremap <M-l> <C-o>l

inoremap <c-a> <c-o>^
inoremap <c-e> <c-o>$

noremap <leader>c "_c
noremap <leader>C "_C
noremap <leader>d "_d
noremap <leader>D "_D

" Faster scrolling with cursor in the middle
nnoremap <M-x> LztM
nnoremap <M-c> HzbM

noremap <C-M-e> 5<C-e>
noremap <C-M-y> 5<C-y>

" noremap <C-M-J> 5<C-e>M
" noremap <C-M-K> 5<C-y>M
noremap <C-M-j> <C-e>M
noremap <C-M-k> <C-y>M

inoremap <M-o> <C-o>o
inoremap <M-O> <C-o>O
inoremap <M-a> <C-o>a
inoremap <M-A> <C-o>A
inoremap <M-p> <C-o>p
inoremap <M-P> <C-o>P

nmap <silent> [l <Plug>(coc-declaration)

function! s:SwitchWinFunc(...)
  wincmd p
  exec "edit " . a:1
endfunction
command! -nargs=* SwitchWinJump call s:SwitchWinFunc(<q-args>)
nn <silent> ]d :call CocActionAsync('jumpDefinition', 'SwitchWinJump')<cr>
nn <silent> ]l :call CocActionAsync('jumpDeclaration', 'SwitchWinJump')<cr>
nn <silent> ]t :call CocActionAsync('jumpTypeDefinition', 'SwitchWinJump')<cr>

nnoremap <silent> <leader>gs :20Git<cr>
nnoremap <silent> <leader>gd :Gdiff<cr>
nnoremap <leader>gdp :Gdiff<space>

" map <C-k> <C-W>k
" map <C-j> <C-W>j
nn <A-n> <c-f>zz
nn <A-m> <c-b>zz
vn <A-n> <c-f>zz
vn <A-m> <c-b>zz
noremap <C-A-u> <C-w><
noremap <C-A-i> <C-w>>
noremap + <C-W>+
noremap - <C-W>-
noremap <A-+> 5<C-W>+
noremap <A--> 5<C-W>-
noremap = <C-W>=
noremap <C-w>; <C-w>p

nmap <leader>o :on<cr>

" NOTE(btolsch): I don't _really_ care about this, but FYI this is because of perf problem when
" closing fugitive diffs.  See https://github.com/tpope/vim-fugitive/issues/1502.
set foldmethod=indent
" set foldmethod=syntax
set foldnestmax=2
set foldlevel=20
nnoremap <space> za

" fix backspace
set bs=indent,eol,start

" remove trailing space
nnoremap <leader>ws :%s/\s\+$//g<CR>

" Visual mode sort
vnoremap <leader>s :sort<CR>

" Better visual mode block indentation
vnoremap > >gv
vnoremap < <gv

vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Tbh I'm not sure why this is x-mode
xnoremap <leader>p "_dP

" Visual copy
nnoremap <leader>y "+y
vnoremap <leader>y "+y
nnoremap <leader>Y "+Y

" Normal paste
nnoremap <C-M-v> "+p

set number
set tw=100
set fo-=t
set fo+=c
set colorcolumn=+1

set hlsearch
set incsearch
set ignorecase
set smartcase
noremap <silent> <leader>/ :nohl<CR>

" Line wrapping
vmap Q gq
nmap Q gqap

set nobackup
set nowritebackup
set noswapfile

" Vimux
noremap <leader>vl :VimuxRunLastCommand<CR>
noremap <leader>vq :VimuxCloseRunner<CR>
noremap <leader>vc :VimuxInterruptRunner<CR>
noremap <leader>vm :VimuxRunCommand("make")<CR>
noremap <leader>vp :VimuxPromptCommand<CR>
noremap <leader>vz :VimuxZoomRunner<CR>
noremap <leader>vo :let g:VimuxRunnerIndex =<space>
noremap <leader>tl :call VimuxSendKeys('c-p')<cr>:call VimuxSendKeys('enter')<cr>
noremap <leader>x :call VimuxSendKeys('c-p')<cr>:call VimuxSendKeys('enter')<cr>
