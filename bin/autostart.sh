#!/bin/sh
source ~/.fehbg
systemctl --user start redshift-gtk
# Don't run xscreensaver on chrome remote desktop
#pidof -x xscreensaver &> /dev/null || [[ "$DISPLAY" != ":0" ]] || xscreensaver -nosplash &
pidof -x dropbox &>/dev/null || dropbox &
pidof -x conky &> /dev/null || conky &
pidof -x udiskie &> /dev/null || udiskie &
#and look into options like ncache and the repainting options
#also maybe want to save a couple past logs...
#x11vnc -display $DISPLAY -usepw -o ~/.vnc/x11vnc.log -forever &>~/.vnc/x11vnc.port &
#disown
