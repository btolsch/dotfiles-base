autocmd! bufwritepost .vimrc source %

let g:powerline_pycmd = 'py3'

" badwolfarch airline theme, not in a git repo so can't use Plugin
set rtp+=~/.config/nvim/custom-airline-themes

" System fzf plugin
set rtp+=/usr/share/vim/vimfiles

call plug#begin('~/.vim/plugged')

Plug 'djoshea/vim-autoread'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-fugitive'
Plug 'christoomey/vim-tmux-navigator'
Plug 'benmills/vimux'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'SirVer/ultisnips'
" TODO: There are probably good ideas here, but I should probably just write my own.
Plug 'honza/vim-snippets'
Plug 'airblade/vim-gitgutter'
Plug 'tmux-plugins/vim-tmux-focus-events'
Plug 'tpope/vim-surround'
" Plug 'b4winckler/vim-angry'
Plug 'tomtom/tcomment_vim'
Plug 'rbgrouleff/bclose.vim'
Plug 'ptzz/lf.vim'
Plug 'voldikss/vim-floaterm'
Plug 'tikhomirov/vim-glsl'
Plug 'easymotion/vim-easymotion'
Plug 'tpope/vim-repeat'
Plug 'haya14busa/incsearch.vim'
Plug 'haya14busa/incsearch-easymotion.vim'
Plug 'haya14busa/incsearch-fuzzy.vim'
Plug 'junegunn/fzf.vim'
Plug 'plasticboy/vim-markdown'
Plug 'jkramer/vim-checkbox'
Plug 'ziglang/zig.vim'
Plug 'sk1418/HowMuch'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/nvim-treesitter-context'
Plug 'nvim-treesitter/nvim-treesitter-textobjects'

Plug 'doums/darcula'
Plug 'nordtheme/vim', {'branch': 'main'}
Plug 'morhetz/gruvbox'

call plug#end()

nnoremap ; :
vnoremap ; :
nnoremap : ;
vnoremap : ;

nnoremap <silent> n nzz
nnoremap <silent> N Nzz
nnoremap <c-d> M<c-d>zz
nnoremap <c-u> M<c-u>zz
nnoremap <c-f> M<c-f>zz
nnoremap <c-b> M<c-b>zz

nnoremap <silent> <space>x :silent !chmod +x %<cr>

nnoremap <leader>as :%s/\<<c-r><c-w>\>/<c-r><c-w>/gI<left><left><left>

set matchpairs+=<:>

set termguicolors
set guicursor=
filetype plugin indent on
syntax on
colorscheme BusyBee_modified
" colorscheme darcula
" colorscheme nord
" colorscheme gruvbox
hi Normal guibg=NONE
set modelines=0

let mapleader = ","
" Leader keybindings
"
" /     :nohl
" bd    delete current buffer but don't close window
" bs    bufdo vimgrepadd @pattern@g | cw
" cc    cclose
" cf    clang format
" co    copen
" cw    cw
" d     "_d
" D     "_D
" e     edit %:h
" gd    :Gdiff
" gdp   :Gdiff for some commit
" gs    :Gstatus
" h     tab previous
" j     tab first
" k     tab last
" l     tab next
" m     max height
" c-m   clear, redraw, make
" n     max width
" c-n   redraw
" o     only
" pc    close preview window
" r     edit .
" s     sort lines
" si    echo syntaxId
" so    source .vimrc
" tl    c-p and enter in runner
" v     select recently pasted text
" vc    vimux interrupt runner
" vl    vimux run last command
" vm    vimux run make
" vo    vimux open runner select
" vp    vimux prompt command
" vq    vimux close runner
" vz    vimux zoom runner
" w     open vertical split window
" wb    <c-w>b
" wp    <c-w>p
" ws    delete trailing whitespace

nnoremap <leader>w :w<cr>
nnoremap <leader>s <c-w>v<c-w>l
nnoremap <leader>v V']
nnoremap <leader>wb <c-w>b
nnoremap <leader>wp <c-w>p
nnoremap <leader>pc :pc<cr>
nnoremap <leader>e :Lf<cr>
nnoremap <leader>le :lopen<cr>
nnoremap <leader>lc :lclose<cr>
nnoremap <silent> K :call CocAction('doHover')<cr>
inoremap <silent> <c-k> <c-o>:call CocActionAsync('showSignatureHelp')<cr>
nmap <silent> <leader>r <Plug>(coc-references)
" inoremap <silent> <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
" inoremap <silent> <expr><S-tab> pumvisible() ? "\<c-p>" : "\<S-tab>"
nnoremap <silent> <leader>sh :call CocActionAsync('showSignatureHelp')<cr>
nnoremap <silent> <leader>p :set paste!<cr>
nmap <silent> [a <Plug>(coc-diagnostic-prev)
nmap <silent> ]a <Plug>(coc-diagnostic-next)

inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

" Make <CR> to accept selected completion item or notify coc.nvim to format
" <C-g>u breaks current undo, please make your own choice.
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#info()['index'] < 0 ? "\<cr>" : coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

nn <silent> xb :call CocLocationsAsync('ccls', '$ccls/inheritance')<cr>
nn <silent> XB :call CocLocationsAsync('ccls', '$ccls/inheritance', {'levels': 3})<cr>
nn <silent> xd :call CocLocationsAsync('ccls', '$ccls/inheritance', {'derived': v:true})<cr>
nn <silent> XD :call CocLocationsAsync('ccls', '$ccls/inheritance', {'derived': v:true}, 'levels': 3)<cr>
nn <silent> xc :call CocLocationsAsync('ccls', '$ccls/call')<cr>
nn <silent> XC :call CocLocationsAsync('ccls', '$ccls/call', {'callee': v:true})<cr>
nn <silent> xt :call CocLocationsAsync('ccls', '$ccls/member', {'kind': 2})<cr>
nn <silent> xf :call CocLocationsAsync('ccls', '$ccls/member', {'kind': 3})<cr>
nn <silent> xm :call CocLocationsAsync('ccls', '$ccls/member')<cr>
nmap <silent> xr <Plug>(coc-rename)

nn <silent> <leader>h :call CocLocations('ccls', '$ccls/navigate', {'direction': 'L'})<cr>
nn <silent> <leader>j :call CocLocations('ccls', '$ccls/navigate', {'direction': 'D'})<cr>
nn <silent> <leader>k :call CocLocations('ccls', '$ccls/navigate', {'direction': 'U'})<cr>
nn <silent> <leader>l :call CocLocations('ccls', '$ccls/navigate', {'direction': 'R'})<cr>

om <leader>a aa
vm <leader>a aa

" BEGIN skipped region code
let g:vimrc_gray_skipped_regions = 1

function! s:ccls_skipped_regions(params) abort
  if g:vimrc_gray_skipped_regions == 0
    return
  endif
  try
    let l:skipped = a:params['skippedRanges']
    let l:bufnr = s:common_notify_checks(a:params['uri'], l:skipped)
    let l:ns = nvim_create_namespace('vimrc_skipped')
    call nvim_buf_clear_namespace(l:bufnr, l:ns, 0, -1)
    for l:range in l:skipped
      try
        let l:start_line = l:range['start']['line']
        let l:end_line = l:range['end']['line']
        for l:line in range(l:start_line + 1, l:end_line - 1)
          call nvim_buf_add_highlight(l:bufnr, l:ns, "Comment", l:line, 0, -1)
        endfor
      endtry
    endfor
  endtry
endfunction
autocmd User CocNvimInit call CocRegistNotification('ccls', '$ccls/publishSkippedRanges', function('s:ccls_skipped_regions'))

function! s:disable_skipped_regions()
  let g:vimrc_gray_skipped_regions = 0
  let l:ns = nvim_create_namespace('vimrc_skipped')
  let l:buffers = getbufinfo()
  for l:b in l:buffers
    let l:bufnr = l:b['bufnr']
    call nvim_buf_clear_namespace(l:bufnr, l:ns, 0, -1)
  endfor
endfunction
command! DisableSkippedRegions call s:disable_skipped_regions()
command! EnableSkippedRegions let g:vimrc_gray_skipped_regions = 1 | exe "CocRestart"

autocmd CursorHold * silent call CocActionAsync('highlight')
autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')

function! s:common_notify_checks(buffer, data) abort
  if type(a:buffer) ==# type("")
    let l:bufnr = s:uri2bufnr(a:buffer)
  elseif type(a:buffer) ==# type(0)
    let l:bufnr = a:buffer
  else
    throw 'buffer must be a string or number'
  endif

  if !bufexists(l:bufnr)
    throw 'buffer does not exist!'
  endif

  if type(a:data) !=# type([])
    throw 'symbols must be a list'
  endif

  return l:bufnr
endfunction

function! s:uri2bufnr(uri) abort
  let l:regex = '\c^[a-z]\+://'

  " Remove the leading file:// or whatever protocol is used
  let l:filename = substitute(a:uri, l:regex, '', '')
  let l:bufnr = bufnr(l:filename)

  if l:bufnr == -1
    " Some characters get escaped by ccls into url encoded format.
    " Only try this if received filename doesn't exist.
    let l:bufnr = bufnr(s:unescape_urlencode(l:filename))
  endif

  return l:bufnr
endfunction

function! s:unescape_urlencode(str) abort
  let l:matches = []
  let l:start = 0

  let l:str = a:str
  while l:start != -1
    let l:match = matchstrpos(l:str, '%[0-9A-Fa-f][0-9A-Fa-f]', l:start)
    let l:start = l:match[2]

    if l:start != -1
      let l:str = l:str[:l:match[1] - 1] . nr2char(str2nr(l:match[0][1:], 16)) .
                  \ l:str[l:match[2]:]
      let l:start = l:match[1] + 1
    endif
  endwhile

  return l:str
endfunction!
" END skipped region code

let g:HowMuch_no_mappings = 1
vmap <a-a> <Plug>AutoCalcAppendWithEq
vmap <a-s> <Plug>AutoCalcReplace

nnore <silent> <space>d :call CocAction('diagnosticToggle')<cr>

call airline#parts#define_function('cocstatus', 'coc#status')
function! ModifyAirlineSections()
  let g:airline_section_x = airline#section#create_right(['cocstatus', 'tagbar', 'filetype'])
endfunction
autocmd User AirlineAfterInit call ModifyAirlineSections()

" autocmd BufEnter * call ncm2#enable_for_buffer()
set completeopt=noinsert,menuone,noselect

autocmd InsertLeave,CompleteDone * if pumvisible() == 0 && &buftype == '' | pclose | endif

nnoremap <silent> <space>p :call fzf#vim#files(expand('%:p:h'), {'source': 'fd -t f -I --hidden --follow', 'options': '--preview "cat {} \| head -200"'}, 1)<cr>
nnoremap <silent> <space>n :Buffers<cr>
nnoremap <silent> <space>s :call fzf#vim#gitfiles('', {'options': '--preview "cat {} \| head -200"'}, 1)<cr>
nnoremap <silent> <space>gs :GFiles!?<cr>
function! s:grep_open(lines)
  if len(a:lines) > 0
    let list = filter(a:lines, 'len(v:val)')
    if !empty(list)
      for line in list
        execute 'edit ' . split(line,':')[0]
      endfor
      let fields = split(list[0],':')
      let filename = fields[0]
      let line = fields[1]
      execute 'edit +' . line . ' ' . filename
    endif
  endif
endfunction
function! s:ggrep(use_project_root, str, fullscreen)
  let @/ = a:str
  let x = 10
  if a:fullscreen == 1
    let x = &lines / 2
  endif
  silent! !git rev-parse
  " NOTE(btolsch): fzf#vim#grep should setup --delimiter and --preview-window offset for us.
  let options = '--preview ' . shellescape('cat {1} | grep --color=always '.shellescape(a:str.'\|$'))
  if a:use_project_root == 1 && v:shell_error == 0
    call fzf#vim#grep(
    \ 'git grep --line-number --color=always '.shellescape(a:str), 0,
    \ { 'dir': systemlist('git rev-parse --show-toplevel')[0],
    \   'options': l:options,
    \   'sink*': function('s:grep_open')}, a:fullscreen)
  else
    call fzf#vim#grep(
    \ 'grep --line-number --color=always '.shellescape(a:str).' . -R', 0,
    \ {
    \   'options': l:options,
    \   'sink*': function('s:grep_open')}, a:fullscreen)
  endif
endfunction
command! -bang -nargs=* GGrep call s:ggrep(1, <q-args>, <bang>0)
command! -bang -nargs=* GGrepLocal call s:ggrep(0, <q-args>, <bang>0)
nn <space>gr :GGrep!<space>
nn <space>gl :GGrepLocal!<space>

function! s:format_quickfix_item(item) abort
  return (a:item.bufnr ? bufname(a:item.bufnr) : '')
        \ . ':' . (a:item.lnum  ? a:item.lnum : '')
        \ . ':' . substitute(a:item.text, '\v^\s*', ' ', '')
endfunction
function! s:fzf_quickfix(fullscreen)
  cclose
  let x = 10
  if a:fullscreen == 1
    let x = &lines / 2
  endif
  call fzf#run(fzf#wrap({
      \ 'source': map(filter(getqflist(), 'v:val["valid"]'), 's:format_quickfix_item(v:val)'),
      \ 'options': '--delimiter : --preview ' . shellescape('cat {1} | hiline {2}') . ' --preview-window +{2}-/2',
      \ 'sink*': function('s:grep_open')
      \ }, a:fullscreen))
endfunction
command! -bang Quickfix call s:fzf_quickfix(<bang>0)
nn <silent> <leader>q :Quickfix<cr>
nn <silent> <space>q :Quickfix!<cr>

let g:fzf_layout = {'down': '~40%'}

imap <c-x><c-f> <plug>(fzf-complete-path)

set shortmess+=c

let g:gitgutter_max_signs = 2000

" inoremap <silent> <expr> <CR> ncm2_ultisnips#expand_or("\<CR>", 'n')
let g:UltiSnipsExpandTrigger = "<C-J>"
let g:UltiSnipsJumpForwardTrigger = "<C-J>"
let g:UltiSnipsJumpBackwardTrigger = "<C-K>"

" incsearch.vim x fuzzy x vim-easymotion
function! s:config_easyfuzzymotion(...) abort
  return extend(copy({
  \   'converters': [incsearch#config#fuzzy#converter()],
  \   'modules': [incsearch#config#easymotion#module()],
  \   'keymap': {"\<CR>": '<Over>(easymotion)'},
  \   'is_expr': 0,
  \   'is_stay': 1
  \ }), get(a:, 1, {}))
endfunction

noremap <silent><expr> <Space>/ incsearch#go(<SID>config_easyfuzzymotion())

" nmap <leader>;         <Plug>(easymotion-repeat)
" map  <leader><leader>j <Plug>(easymotion-sol-bd-jk)
" map  <leader>j         <Plug>(easymotion-bd-jk)
" map          /         <Plug>(easymotion-sn)
" omap         /         <Plug>(easymotion-tn)
" map  <leader>f         <Plug>(easymotion-bd-f2)
" nmap <leader>f         <Plug>(easymotion-overwin-f2)
" map  <leader>t         <Plug>(easymotion-bd-t)
map          f         <Plug>(easymotion-bd-fl)
map          t         <Plug>(easymotion-bd-tl)
" map  <leader>h         <Plug>(easymotion-lineanywhere)
" map  <leader>l         <Plug>(easymotion-lineanywhere)
" map  <leader>w         <Plug>(easymotion-bd-w)
" nmap <leader>w         <Plug>(easymotion-overwin-w)

let g:EasyMotion_use_upper = 1
let g:EasyMotion_keys = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ;'
let g:EasyMotion_smartcase = 1
let g:EasyMotion_use_smartsign_us = 1
let g:EasyMotion_startofline = 0

nn <silent> JK :exe "e " . expand("<cWORD>")<cr>

" Copynpaste out of vim-tmux-navigator.
function! s:TmuxSocket()
  " The socket path is the first value in the comma-separated list of $TMUX.
  return split($TMUX, ',')[0]
endfunction
function! s:TmuxCommand(args)
  let cmd = 'tmux -S ' . s:TmuxSocket() . ' ' . a:args
  let l:x=&shellcmdflag
  let &shellcmdflag='-c'
  let retval=system(cmd)
  let &shellcmdflag=l:x
  return retval
endfunction

if empty($TMUX)
  nn <silent> <leader>b :let @+ = 'b ' . expand('%:p') . ':' . line('.')<cr>
else
  nn <silent> <leader>b :call <SID>TmuxCommand('set-buffer "b ' . expand('%:p') . ':' . line('.') . '"')<cr>
endif
autocmd TextYankPost * silent! if v:event["regname"] == "+" | call system("tmux loadb -", join(v:event["regcontents"], "\n")) | endif

let g:ranger_map_keys = 0
let g:lf_map_keys = 0
let g:lf_replace_netrw = 1
let g:lf_width = 0.95
let g:lf_height = 0.95

let g:python_host_prog = '/usr/bin/python2'
let g:python3_host_prog = '/usr/bin/python3'

let g:airline_theme = 'dark_modified'

let g:vim_markdown_no_default_key_mappings = 1
let g:vim_markdown_new_list_item_indent = 2

set previewheight=25
set timeoutlen=150 ttimeoutlen=0

set omnifunc=syntaxcomplete#Complete

nnoremap <leader>bs :cex []<bar>execute 'bufdo vimgrepadd @@g %'<bar>cw<s-left><s-left><right>

hi link cDefine PreProc
hi link cInclude PreProc
hi link cPrecondit PreProc

hi link pythonInclude PreProc

hi! link FoldColumn LineNr

hi link qfFileName PreProc
hi link qfLineNr Number
hi link QuickFixLine CursorLine

set updatetime=1000
hi CocHighlightText guibg=#3e4a82 ctermbg=103

set cursorline
set relativenumber number

autocmd FocusLost,WinLeave,InsertEnter * set norelativenumber number
autocmd FocusGained,WinEnter,InsertLeave * set relativenumber number

set fillchars+=vert:\ " comment

nnoremap <leader>si :call SyntaxItem()<CR>
function! SyntaxItem()
  echo synIDattr(synID(line("."), col("."), 1), "name")
endfunction

nnoremap j gj
nnoremap k gk

set hidden
set confirm
set splitbelow
set splitright

set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set cindent
set cino=N-s,g0,(0,W2s,j1 " ,+2s

" autocmd FileType markdown call ncm2#disable_for_buffer()
" autocmd FileType text call ncm2#disable_for_buffer()
" autocmd FileType gitcommit call ncm2#disable_for_buffer()
autocmd FileType text setlocal nocindent autoindent fo=t
autocmd FileType gitcommit setlocal tw=72 nocindent autoindent fo=t
autocmd FileType make setlocal noexpandtab
autocmd FileType vim setlocal fdc=1
autocmd FileType vim setlocal foldlevel=0
autocmd FileType vim setlocal foldmethod=marker
autocmd BufNewFile,BufRead /tmp/mutt* setlocal autoindent nocindent filetype=mail tw=80 digraph
autocmd BufNewFile,BufRead ~/tmp/mutt* setlocal autoindent nocindent filetype=mail tw=80 digraph
autocmd BufNewFile,BufRead *.vs,*.gs,*.fs set filetype=glsl

nn <silent> <leader>so :source $MYVIMRC<cr>

nnoremap <silent> <leader>cf :call FormatAll()<CR>
execute 'vnoremap <silent> <leader>cf :py3f ' . clang_format_py . '<CR>'

let s:clang_format_py = clang_format_py
function! FormatAll()
  let l:lines="all"
  execute 'py3f ' . s:clang_format_py
endfunction

nnoremap <silent> <leader>gf :pyf $HOME/.vim/plugged/gn/misc/vim/gn-format.py<cr>

call tcomment#type#Define('c', tcomment#GetLineC('// %s'))

set makeprg=./build.sh
set errorformat=%f:%l:%c:\ error:\ %m,%f:%l:%c:\ fatal\ error:\ %m
nnoremap <silent> <leader>bl :make<cr>
nnoremap <silent> <leader>z :make<cr>

inoremap <c-c> TODO(btolsch):<space>
inoremap <c-b> NOTE(btolsch):<space>

nnoremap <silent> <M-h> :bprevious<CR>
nnoremap <silent> <M-l> :bnext<CR>
nnoremap <silent> <M-j> :tabprevious<CR>
nnoremap <silent> <M-k> :tabnext<CR>
nnoremap <silent> <M-;> :b#<cr>
nnoremap <silent> [q :cprev<cr>zz
nnoremap <silent> ]q :cnext<cr>zz
nnoremap <silent> <leader>co :copen<cr>
nnoremap <silent> <leader>cc :cclose<cr>
nnoremap <silent> <leader>cw :cw<cr>

nmap [c <Plug>(GitGutterPrevHunk)zz
nmap ]c <Plug>(GitGutterNextHunk)zz

nnoremap <silent> <leader>m <C-w>_
nnoremap <silent> <leader>n <C-w>\|

nnoremap <silent> <M-<> :tabm -1<CR>
nnoremap <silent> <M->> :tabm +1<CR>
nnoremap <C-M-n> :tab split<cr>

" Insert home key motion with Alt
inoremap <M-h> <C-o>h
inoremap <M-j> <C-o>j
inoremap <M-k> <C-o>k
inoremap <M-l> <C-o>l

inoremap <c-a> <c-o>^
inoremap <c-e> <c-o>$

noremap <leader>c "_c
noremap <leader>C "_C
noremap <leader>d "_d
noremap <leader>D "_D

" Faster scrolling with cursor in the middle
nnoremap <M-x> LztM
nnoremap <M-c> HzbM

noremap <C-M-e> 5<C-e>
noremap <C-M-y> 5<C-y>

inoremap <M-o> <C-o>o
inoremap <M-O> <C-o>O
inoremap <M-a> <C-o>a
inoremap <M-A> <C-o>A
inoremap <M-p> <C-o>p
inoremap <M-P> <C-o>P

nmap <silent> [d <Plug>(coc-definition)
nmap <silent> [l <Plug>(coc-declaration)
nmap <silent> [t <Plug>(coc-type-definition)

function! s:SwitchWinFunc(...)
  wincmd p
  exec "edit " . a:1
endfunction
command! -nargs=* SwitchWinJump call s:SwitchWinFunc(<q-args>)
nn <silent> ]d :call CocActionAsync('jumpDefinition', 'SwitchWinJump')<cr>
nn <silent> ]l :call CocActionAsync('jumpDeclaration', 'SwitchWinJump')<cr>
nn <silent> ]t :call CocActionAsync('jumpTypeDefinition', 'SwitchWinJump')<cr>

nmap <M-1> <Plug>AirlineSelectTab1
nmap <M-2> <Plug>AirlineSelectTab2
nmap <M-3> <Plug>AirlineSelectTab3
nmap <M-4> <Plug>AirlineSelectTab4
nmap <M-5> <Plug>AirlineSelectTab5
nmap <M-6> <Plug>AirlineSelectTab6
nmap <M-7> <Plug>AirlineSelectTab7
nmap <M-8> <Plug>AirlineSelectTab8
nmap <M-9> <Plug>AirlineSelectTab9

nnoremap <silent> <leader>gs :20Git<cr>
nnoremap <silent> <leader>gd :Gdiff<cr>
nnoremap <leader>gdp :Gdiff<space>

" map <C-k> <C-W>k
" map <C-j> <C-W>j
nn <A-n> <c-f>zz
nn <A-m> <c-b>zz
vn <A-n> <c-f>zz
vn <A-m> <c-b>zz
" noremap <A-n> <C-w><
" noremap <A-m> <C-w>>
noremap + <C-W>+
noremap - <C-W>-
noremap <A-+> 5<C-W>+
noremap <A--> 5<C-W>-
noremap = <C-W>=
noremap <C-w>; <C-w>p

nmap <leader>o :on<cr>

" NOTE(btolsch): I don't _really_ care about this, but FYI this is because of perf problem when
" closing fugitive diffs.  See https://github.com/tpope/vim-fugitive/issues/1502.
set foldmethod=indent
" set foldmethod=syntax
set foldnestmax=2
set foldlevel=20
nnoremap <space> za

" fix backspace
set bs=indent,eol,start

" remove trailing space
nnoremap <leader>ws :%s/\s\+$//g<CR>

" Visual mode sort
vnoremap <leader>s :sort<CR>

" Better visual mode block indentation
vnoremap > >gv
vnoremap < <gv

vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Tbh I'm not sure why this is x-mode
xnoremap <leader>p "_dP

" Visual copy
nnoremap <leader>y "+y
vnoremap <leader>y "+y
nnoremap <leader>Y "+Y

" Normal paste
nnoremap <C-M-v> "+p

set number
set tw=100
set fo-=t
set fo+=c
set colorcolumn=+1

set hlsearch
set incsearch
set ignorecase
set smartcase
noremap <silent> <leader>/ :nohl<CR>

" Line wrapping
vmap Q gq
nmap Q gqap

set nobackup
set nowritebackup
set noswapfile

" Vimux
noremap <leader>vl :VimuxRunLastCommand<CR>
noremap <leader>vq :VimuxCloseRunner<CR>
noremap <leader>vc :VimuxInterruptRunner<CR>
noremap <leader>vm :VimuxRunCommand("make")<CR>
noremap <leader>vp :VimuxPromptCommand<CR>
noremap <leader>vz :VimuxZoomRunner<CR>
noremap <leader>vo :let g:VimuxRunnerIndex =<space>
noremap <leader>tl :call VimuxSendKeys('c-p')<cr>:call VimuxSendKeys('enter')<cr>
noremap <leader>x :call VimuxSendKeys('c-p')<cr>:call VimuxSendKeys('enter')<cr>

" Bufferline and Airline
" https://github.com/bling/vim-bufferline.git
" https://github.com/bling/vim-airline.git
set laststatus=2
set noshowmode
let g:bufferline_echo = 0
let g:bufferline_active_buffer_left = '['
let g:bufferline_active_buffer_right = ']'
let g:bufferline_rotate = 1
let g:bufferline_fixed_index = 0

" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 1
" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'
" let g:airline_theme="badwolfarch"
let g:airline_detect_modified = 1

let g:airline_powerline_fonts = 1

lua << TURTLES
require'treesitter-context'.setup({
  enable = true, -- Enable this plugin (Can be enabled/disabled later via commands)
  max_lines = -1, -- How many lines the window should span. Values <= 0 mean no limit.
  min_window_height = 0, -- Minimum editor window height to enable context. Values <= 0 mean no limit.
  line_numbers = true,
  multiline_threshold = 20, -- Maximum number of lines to show for a single context
  trim_scope = 'outer', -- Which context lines to discard if `max_lines` is exceeded. Choices: 'inner', 'outer'
  mode = 'cursor',  -- Line used to calculate context. Choices: 'cursor', 'topline'
  -- Separator between context and content. Should be a single character string, like '-'.
  -- When separator is set, the context will only show up when there are at least 2 lines above cursorline.
  separator = nil,
  zindex = 20, -- The Z-index of the context window
  on_attach = nil, -- (fun(buf: integer): boolean) return false to disable attaching
})

vim.keymap.set("n", "[u", function() require("treesitter-context").go_to_context(vim.v.count1) end, { silent = true })
TURTLES

lua << TURTLES
require'nvim-treesitter.configs'.setup({
  -- A list of parser names, or "all" (the five listed parsers should always be installed)
  ensure_installed = { "c", "lua", "vim", "vimdoc", "query" },

  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- Automatically install missing parsers when entering buffer
  -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
  auto_install = false,

  -- List of parsers to ignore installing (or "all")
  -- ignore_install = { "javascript" },

  ---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
  -- parser_install_dir = "/some/path/to/store/parsers", -- Remember to run vim.opt.runtimepath:append("/some/path/to/store/parsers")!

  highlight = {
    enable = false,

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    -- additional_vim_regex_highlighting = false,
  },
})
TURTLES

lua <<EOF
require'nvim-treesitter.configs'.setup({
  textobjects = {
    select = {
      enable = true,

      -- Automatically jump forward to textobj, similar to targets.vim
      lookahead = true,

      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ["af"] = "@function.outer",
        ["if"] = "@function.inner",
        ["aa"] = "@parameter.outer",
        ["ia"] = "@parameter.inner",
        -- You can also use captures from other query groups like `locals.scm`
        ["as"] = { query = "@scope", query_group = "locals", desc = "Select language scope" },
      },
      -- You can choose the select mode (default is charwise 'v')
      --
      -- Can also be a function which gets passed a table with the keys
      -- * query_string: eg '@function.inner'
      -- * method: eg 'v' or 'o'
      -- and should return the mode ('v', 'V', or '<c-v>') or a table
      -- mapping query_strings to modes.
      selection_modes = {
        ['@parameter.outer'] = 'v', -- charwise
        ['@function.outer'] = 'V', -- linewise
        ['@class.outer'] = '<c-v>', -- blockwise
      },
      -- If you set this to `true` (default is `false`) then any textobject is
      -- extended to include preceding or succeeding whitespace. Succeeding
      -- whitespace has priority in order to act similarly to eg the built-in
      -- `ap`.
      --
      -- Can also be a function which gets passed a table with the keys
      -- * query_string: eg '@function.inner'
      -- * selection_mode: eg 'v'
      -- and should return true of false
      include_surrounding_whitespace = function(args)
        if args["query_string"] == "@parameter.outer" or args["query_string"] == "@function.outer" then
          return true
        end
        return false
      end,
    },
    swap = {
      enable = true,
      swap_next = {
        ["[]"] = "@parameter.inner",
      },
      swap_previous = {
        ["]["] = "@parameter.inner",
      },
    },
    move = {
      enable = true,
      set_jumps = false, -- whether to set jumps in the jumplist
      goto_next_start = {
        ["]m"] = "@parameter.inner",
        ["]s"] = { query = "@scope", query_group = "locals", desc = "Next scope" },
        ["]f"] = "@function.outer",
      },
      goto_next_end = {
        ["}M"] = "@parameter.inner",
        ["}S"] = { query = "@scope", query_group = "locals", desc = "Next scope" },
      },
      goto_previous_start = {
        ["[m"] = "@parameter.inner",
        ["[s"] = { query = "@scope", query_group = "locals", desc = "Next scope" },
        ["[f"] = "@function.outer",
      },
      goto_previous_end = {
        ["{M"] = "@parameter.inner",
        ["{S"] = { query = "@scope", query_group = "locals", desc = "Next scope" },
      },
    },
  },
})
vim.keymap.set('n', '}F', ']fzz', {remap = true})
vim.keymap.set('n', '}F', ']fzz', {remap = true})
EOF
