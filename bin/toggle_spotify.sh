#!/bin/bash

if [[ -e ~/.spotify ]]; then
  rm -f ~/.spotify
else
  touch ~/.spotify
fi
