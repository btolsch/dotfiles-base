#!/bin/zsh

backup_check_hostname() {
  if [[ $(hostname) != $1 ]]; then
    echo "Actual hostname ($(hostname)) doesn't match given hostname ($1).  If this is intended, add a work-around or rsync manually." >&2
    exit 1
  fi
}

backup_dir_nocheck() {
  echo "$HOME/epicurus/backup/staging/$1/$2"
}

touch_stages_txt() {
  STAGES="$(dirname $1)/stages.txt"
  N=$(basename $1)
  if [[ -f $STAGES ]]; then
    if [[ $N -gt 0 ]]; then
      PREV=$((N-1))
      DATE=$(sed -n "s/^$PREV \(.*\)$/\1/p" $STAGES)
    else
      DATE=$(date '+%Y/%m/%d')
    fi
    sed -i "s#^$N .*\$#$N $DATE#" $STAGES
  fi
}

backup_dir() {
  backup_check_hostname $1
  backup_dir_nocheck $1 $2
}

backup_base() {
  SRC_DIR=$1
  DEST_DIR=$2
  touch_stages_txt $DEST_DIR
  sudo zsh -s << EOF
    if [[ -e $DEST_DIR && -e $SRC_DIR ]]; then
      rsync -avx --delete ${(@)@[@]:3} $SRC_DIR $DEST_DIR
    elif mount | grep -q "epicurus/backup" &>/dev/null; then
      echo "necessary backup directories don't exist?"
    else
      echo "epicurus backup not mounted?"
    fi
EOF
}

backup_depthn_base() {
  HOST=$1
  N=$2

  if [[ -z $1 ]]; then
    echo "$0: host directory name is empty"
    exit 1
  fi
  if [[ -z $N || $N < 1 ]]; then
    echo "$0: invalid N ($N)"
    exit 1
  fi
  shift; shift

  DEST=$N
  SRC=$((N-1))
  DEST_DIR=$(backup_dir_nocheck $HOST $DEST)
  SRC_DIR=$(backup_dir_nocheck $HOST $SRC)

  backup_base $SRC_DIR/ $DEST_DIR $@
}
