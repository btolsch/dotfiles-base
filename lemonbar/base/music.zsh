#!/bin/zsh

BAR_DIR=$(dirname $0)

. $BAR_DIR/common.sh

if [[ -e ~/.spotify ]]; then
  source ~/.config/spotify
  PLAYBACK_RESULT=$(curl -X "GET" "https://api.spotify.com/v1/me/player/currently-playing" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer $ACCESS_TOKEN" --silent)
  if [[ $(echo "$PLAYBACK_RESULT" | jq '. | has("error")') == "true" ]]; then
    echo -n "$(echo $PLAYBACK_RESULT | tr -d '\r\n')"
  else
    if [[ $(echo "$PLAYBACK_RESULT" | jq '.is_playing') == "true" ]]; then
      echo -en "%{F$BLUE}\ue0fe%{F-} "
    else
      echo -en "%{F$BLUE}\ue09b%{F-} "
    fi
    IFS=$'\n' lines=($(echo "$PLAYBACK_RESULT" | jq '.item.artists[0].name, .item.name, ((.progress_ms/1000 | floor)|((./60|floor),.%60)), ((.item.duration_ms/1000|ceil)|((./60|floor),.%60))' | sed 's/^"\(.*\)"$/\1/'))
    echo -en "${lines[1]} - ${lines[2]}$SEP${lines[3]}:$(printf '%02d' ${lines[4]})/${lines[5]}:$(printf '%02d' ${lines[6]})"
    echo -n " %{F$BLUE}%{I$DOTFILES_DIR/icons/eq.xbm}%{F-} "
  fi
else
  MPC_LINES=$(mpc status 2>/dev/null)
  MPC_LINES=(${(f)MPC_LINES})
  if [ ${#MPC_LINES} -le 1 ]; then
    exit
  fi
  if echo ${MPC_LINES[2]} | grep -q '^\[playing\]'; then
    echo -en "%{F$BLUE}\ue0fe%{F-} "
  else
    echo -en "%{F$BLUE}\ue09b%{F-} "
  fi
  echo -en "${MPC_LINES[1]}$SEP$(echo ${MPC_LINES[2]} | awk '{print $3}')"
  echo -n " %{F$BLUE}%{I$DOTFILES_DIR/icons/eq.xbm}%{F-} "

  if [ $(pulseaudio-ctl full-status | cut -f 2 -d ' ') = "yes" ]; then
    echo -en "%{F$BLUE}\ue202%{F-} "
  else
    echo -en "%{F$BLUE}\ue203%{F-} "
  fi
  echo -en "$(echo ${MPC_LINES[3]} | sed 's/^volume:\s*\([0-9]\+%\|n\/a\).*$/\1/')"
fi
