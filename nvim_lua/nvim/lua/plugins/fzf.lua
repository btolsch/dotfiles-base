local fzf = require("fzf-lua")

fzf.setup({
  winopts = {
    width = 1.0,
    height = 0.4,
    row  = 1.0,
    preview = {
      horizontal = 'right:50%',
    },
  },
  previewers = {
    builtin = {
      syntax = true,
      treesitter = { enable = false },
    },
  },
  fzf_opts = {
    ['--layout'] = 'default',
  },

  files = {
    actions = {
      ["default"] = require("fzf-lua.actions").file_edit,
    },
  },
  git = {
    bcommits = {
      prompt = "logs:",
      cmd = "git log --color --pretty=format:'%C(yellow)%h%Creset %Cgreen%><(12)%cr%><|(12)%Creset %s' <file>",
      preview = "git show --stat --color --format='%C(cyan)%an%C(reset)%C(bold yellow)%d%C(reset): %s' {1} -- <file>",
      actions = {
        ["ctrl-d"] = function(...)
          fzf.actions.git_buf_vsplit(...)
          vim.cmd("windo diffthis")
          local switch = vim.api.nvim_replace_termcodes("<C-w>h", true, false, true)
          vim.api.nvim_feedkeys(switch, "t", false)
        end,
      },
    },
    files = {
      actions = {
        ["default"] = require("fzf-lua.actions").file_edit,
      },
    },
  },
  grep = {
    actions = {
      ["default"] = require("fzf-lua.actions").file_edit,
    },
  },
  quickfix = {
    only_valid = true,
  },
})

vim.keymap.set("n", "<space>n", function() fzf.buffers() end)
vim.keymap.set("n", "<space>s", function() fzf.git_files({winopts = { fullscreen = 1 }}) end)
vim.keymap.set("n", "<space>pl", function()
  fzf.files({
    winopts = { fullscreen = 1 },
    fd_opts = [[--color=never --type f --hidden --no-ignore --follow ]]
  })
end)
vim.keymap.set("n", "<space>p", function()
  local result = vim.system({'git', 'rev-parse', '--show-toplevel'}):wait()
  local opts = {
    winopts = { fullscreen = 1 },
    fd_opts = [[--color=never --type f --hidden --no-ignore --follow ]]
  }
  if result.code == 0 then
    opts.cwd = vim.trim(result.stdout)
  end
  fzf.files(opts)
end)

vim.keymap.set("n", "<space>gs", function() fzf.git_status({winopts = { fullscreen = 1 }}) end)

vim.keymap.set("n", "<space>gr", function()
  local result = vim.system({'git', 'rev-parse', '--show-toplevel'}):wait()
  local opts = {
    winopts = { fullscreen = 1 },
  }
  if result.code == 0 then
    opts.cwd = vim.trim(result.stdout)
    opts.cmd = "git grep --color=always --line-number --column"
  end
  fzf.grep(opts)
end)
vim.keymap.set("n", "<space>gl", function()
  fzf.grep({
    -- TODO(btolsch): Want --full-name?
    cmd = "git grep --color=always --line-number --column",
    winopts = { fullscreen = 1 },
  })
end)
vim.keymap.set("n", "<space>gw", function()
  local result = vim.system({'git', 'rev-parse', '--show-toplevel'}):wait()
  local opts = {
    winopts = { fullscreen = 1 },
  }
  if result.code == 0 then
    opts.cwd = vim.trim(result.stdout)
    opts.cmd = "git grep --color=always --line-number --column"
  end
  fzf.grep_cword(opts)
end)

vim.keymap.set("n", "<space>lg", function() fzf.git_bcommits() end)

vim.keymap.set("n", "<space>q", function()
  vim.cmd [[ cclose ]]
  fzf.quickfix({winopts = { fullscreen = 1 }})
end)
vim.keymap.set("n", "<leader>q", function()
  vim.cmd [[ cclose ]]
  fzf.quickfix()
end)

vim.keymap.set("i", "<c-x><c-f>", function()
  local result = vim.system({'git', 'rev-parse', '--show-toplevel'}):wait()
  local opts = {}
  if result.code == 0 then
    opts.cmd = "fd --hidden --no-ignore --base-directory " .. vim.trim(result.stdout)
  end
  fzf.complete_path(opts)
end)
