local client = vim.lsp.start({
  name = 'ccls',
  cmd = {'ccls', '--log-file=/tmp/ccls.log'},
  root_dir = vim.fs.dirname(vim.fs.find({".ccls", "compile_commands.json", ".git/"}, { upward = true })[1]),
  init_options = {
    cache = {
      directory = "/home/btolsch/.cache/ccls",
    },
  },
})

vim.lsp.buf_attach_client(0, client)

vim.keymap.set('i', '`', '->', {buffer})
vim.keymap.set('i', '``', '`', {buffer})

local use_skip_ranges = true
local ns = vim.api.nvim_create_namespace("ccls_skipped")

-- NOTE(btolsch): To actually get the skipped ranges back, just write the file again.
vim.keymap.set('n', '<space>k',
  function()
    use_skip_ranges = not use_skip_ranges
    if not use_skip_ranges then
      vim.api.nvim_buf_clear_namespace(vim.api.nvim_get_current_buf(), ns, 0, -1)
    end
  end,
  {buffer})

vim.api.nvim_create_autocmd("LspAttach", {
  callback = function(args)
    local function ccls_call(callee)
      return function()
        local function handler(err, result)
          local qf = {};
          for i,v in ipairs(result) do
            local line = v.range.start.line
            qf[i] = {
              filename = string.sub(v.uri, 6),
              lnum = line + 1,
              text = vim.api.nvim_buf_get_lines(0, line, line + 1, false)[1],
            }
          end
          vim.fn.setqflist(qf)
          if #qf > 0 then
            vim.cmd("cw " .. string.format("%d", math.min(#qf, 10)))
          end
          -- print(vim.inspect(err))
          -- print(vim.inspect(result))
        end
        local file = "file://" .. vim.fn.expand("%:p")
        local line = vim.fn.line('.') - 1
        local char = vim.fn.virtcol('.') - 1
        vim.lsp.get_active_clients()[1].request(
            "$ccls/call",
            {
              callee = callee,
              textDocument = { uri = file },
              position = { line = line, character = char },
            }, handler, 0)
      end
    end
    vim.keymap.set('n', 'xc', ccls_call(false))
    vim.keymap.set('n', 'XC', ccls_call(true))

    vim.lsp.handlers["$ccls/publishSkippedRanges"] = function(err, result, ctx, config)
      local bufnr = vim.fn.bufnr(string.sub(result.uri, 6))
      if bufnr >= 0 and use_skip_ranges then
        vim.api.nvim_buf_clear_namespace(bufnr, ns, 0, -1)
        for _,range in ipairs(result.skippedRanges) do
          for i=range.start.line+1,range["end"].line-1 do
            vim.api.nvim_buf_add_highlight(bufnr, ns, "Comment", i, 0, -1)
          end
        end
      end
    end
  end
})
