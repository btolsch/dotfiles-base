#!/bin/zsh

battery_lines() {
  local BAT=(${(s:, :)$(acpi -d "$(realpath /sys/class/power_supply/BAT0/device)" | sed 's/[^:]*: //')})
  eval $1"=(\${BAT})"
}
