#
# wm independent hotkeys
#

# terminal emulator
super + Return
  alacritty
super + ctrl + Return
  (wid=$(bspc subscribe -c 1 node_add | awk '\{ print $5 \}'); xdotool windowmove --relative $wid 500 -300 windowsize $wid 700 700) &; st -c floating -e calc.sh

# program launcher
super + d
  rofi -disable-history -show run

# make sxhkd reload its configuration files:
super + Escape
  pkill -USR1 -x sxhkd

super + shift + Escape
  kill-lemonbar.sh; ~/dotfiles/lemonbar/bspwm/bar.zsh&

#
# bspwm hotkeys
#

# quit/restart bspwm
super + alt + q
  confirm.sh "really quit?" "bspc quit"
super + alt + r
  bspc wm -r

# close and kill
super + {_,shift + }q
  bspc node -{c,k}

# alternate between the tiled and monocle layout
super + m
  bspc desktop -l next

# send the newest marked node to the newest preselected node
super + ctrl + y
  bspc node newest.marked.local -n newest.!automatic

# send the focused node to the newest preselected node
super + y
  bspc node -n newest.!automatic

# swap the current node and the biggest node
super + g
  bspc node -s biggest.local

super + s
  bspc node -s older

#
# state/flags
#

# set the window state
super + {t,shift + t,f}
  bspc node -t {tiled,pseudo_tiled,'~fullscreen'}
super + shift + space
  bspc query -N -n focused.floating &> /dev/null && bspc node -t tiled || bspc node -t floating
super + space
  bspc query -N -n focused.tiled >/dev/null && bspc node -f next.floating.local || bspc node -f next.tiled.local

# set the node flags
super + ctrl + {m,x,y,z}
  bspc node -g {marked,locked,sticky,private}

#
# focus/swap
#

# focus the node in the given direction
super + {_,shift + }{h,j,k,l}
  bspc node -{f,s} {west,south,north,east}

# focus the node for the given path jump
super + {p,b,comma,period}
  bspc node -f @{parent,brother,first,second}

# focus the next/previous node in the current desktop
super + {_,shift + }Tab
  bspc node -f {next,prev}.local

# focus the next/previous desktop
super + alt + {h,l}
  bspc desktop -f {prev,next}.local
super + bracket{left,right}
  bspc desktop -f {prev,next}

# focus the last node/desktop
super + {semicolon,slash}
  bspc {node,desktop} -f last

# focus the older or newer node in the focus history
super + {o,i}
  bspc wm -h off; \
  bspc node {older,newer} -f; \
  bspc wm -h on

# focus or send to the given desktop
super + {_,shift + }{1-9,0}
  bspc {desktop -f,node -d} '{1-9,10}'

super + alt + {_,shift + }{1-9,0}
  bspc {desktop -f,node -d} 'f{1-9,10}'

#
# preselect
#

# preselect the direction
super + ctrl + {h,j,k,l}
  bspc node -p {west,south,north,east}

# preselect the ratio
super + ctrl + {1-9}
  bspc node -o 0.{1-9}

# cancel the preselection for the focused node
super + ctrl + space
  bspc node -p cancel

# cancel the preselection for the focused desktop
super + ctrl + shift + space
  bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

#
# move/resize
#

# expand a window by moving one of its side outward
super + r : {h,j,k,l,alt+h,alt+j,alt+k,alt+l}
  bspc node -z {right -20 0,bottom 0 20,bottom 0 -20,right 20 0,right -100 0,bottom 0 100,bottom 0 -100,right 100 0}
super + alt + m : {h,j,k,l,alt+h,alt+j,alt+k,alt+l}
  bspc node -v {-20 0,0 20,0 -20,20 0,-100 0,0 100,0 -100,100 0}

# move a floating window
super + {Left,Down,Up,Right}
  bspc node -v {-20 0,0 20,0 -20,20 0}

# rotate tree left/right
super + {braceleft,braceright}
  bspc node @parent -R {270,90}

super + c
  gsimplecal

super + backslash
  firefox

XF86AudioRaiseVolume
  vol_dzen 1
XF86AudioLowerVolume
  vol_dzen -1
XF86AudioMute
  pulseaudio-ctl mute

XF86AudioNext
  mpc next
XF86AudioPrev
  mpc prev
XF86AudioPlay
  mpc toggle
super + XF86AudioPlay
  xdotool key --window "$(xdotool search --class mpv)" p

XF86MonBrightnessUp
  ~/bin/brightness.sh -inc 10
XF86MonBrightnessDown
  ~/bin/brightness.sh -dec 10

XF86Eject
  xdotool click --repeat 3 --delay 10 1

super + alt + s
  ~/bin/toggle_spotify.sh

ctrl + alt + slash
  ~/bin/current-sink.sh

super + ctrl + w
  wall.sh

super + alt + x
  hdmi.sh

super + alt + slash
  bspc monitor -f last

super + shift + g : {plus,minus}
  bspc config window_gap $(( $(bspc config window_gap) {+,-}5))

super + alt + t
  ~/dotfiles/base/lemonbar/sxhkd/pass-through-toggle.sh

super + ctrl + s
  xscreensaver-toggle &>/dev/null

super + minus
  tdrop -ma -w -2 -y 1 -f ~/bin/dropdown -n dropdown st
super + grave
  tdrop -ma -w 32% -h 80% -y 1 -f ~/bin/scratch -n scratch st

super + shift + o
  ~/bin/locker.sh

super + shift + minus
  pkill dzen2

# Make all windows open on currently focused desktop.
super + ctrl + r
  bspc rule -a \* desktop=$(bspc wm -g | sed "s/^$(bspc config status_prefix)/:/" | sed 's/^.*\(:M.*$\)/\1/' | sed 's/:m.*$//' | sed 's/^.*:\(F\|O\)\([^:]*\):.*$/\2/')

# Clear the above window open rule.
super + ctrl + c
  (FOO=$(bspc rule -l | grep -n -E '^\*:\*:\* => desktop=' | sed 's/^\([0-9]*\):.*$/\1/' | head -1); [[ $FOO -gt 0 ]] && bspc rule -r \^$FOO)
