let g:insert_checkbox = '- '
let g:insert_checkbox_prefix = '- '

nn <buffer> <silent> <tab> :.HeaderIncrease<cr>
nn <buffer> <silent> <s-tab> :.HeaderDecrease<cr>
vn <buffer> <silent> <tab> :HeaderIncrease<cr>
vn <buffer> <silent> <s-tab> :HeaderDecrease<cr>

nmap <buffer> [h <Plug>Markdown_MoveToPreviousHeader<bar>zz
nmap <buffer> ]h <Plug>Markdown_MoveToNextHeader<bar>zz
nmap <buffer> [s <Plug>Markdown_MoveToPreviousSiblingHeader<bar>zz
nmap <buffer> ]s <Plug>Markdown_MoveToNextSiblingHeader<bar>zz
nmap <buffer> [u <Plug>Markdown_MoveToParentHeader<bar>zz
nmap <buffer> <leader>ch <Plug>Markdown_MoveToCurHeader)<bar>zz
vmap <buffer> [h <Plug>Markdown_MoveToPreviousHeader
vmap <buffer> ]h <Plug>Markdown_MoveToNextHeader
vmap <buffer> [s <Plug>Markdown_MoveToPreviousSiblingHeader
vmap <buffer> ]s <Plug>Markdown_MoveToNextSiblingHeader
vmap <buffer> [u <Plug>Markdown_MoveToParentHeader
vmap <buffer> <leader>ch <Plug>Markdown_MoveToCurHeader
nmap <buffer> gx <Plug>Markdown_OpenUrlUnderCursor
nmap <buffer> ge <Plug>Markdown_EditUrlUnderCursor

nn <buffer> <silent> <space>t :ToggleCB<cr>
ino <buffer> <silent> <c-m-c> <esc>:ToggleCB<cr>A

set tw=100
