#!/bin/bash
sed -i '/ACCESS_TOKEN/d' ~/.config/spotify
AUTH_RESULT=$(spotify_auth.sh)
TOKEN_RESULT=$(echo "$AUTH_RESULT" | sed -n '/^ACCESS_TOKEN=/p')
if [[ -z $TOKEN_RESULT ]]; then
  echo "auth error"
  echo $AUTH_RESULT
  exit 1
else
  echo "$TOKEN_RESULT" >> ~/.config/spotify
fi
