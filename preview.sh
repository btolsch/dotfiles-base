#!/bin/bash

case "$1" in
  *.png|*.jpg|*.mp4|*.mp3|*.mkv) exiftool "$1";;
  *.md) which glow &>/dev/null && glow -w 102 -s dark "$1" || cat "$1";;
  *.pdf) pdftotext "$1" -;;
  *.tar.bz|*.tar.bz2|*.tbz|*.tbz2) tar tjf "$1";;
  *.tar.gz|*.tgz) tar tzf "$1";;
  *.tar.xz|*.txz) tar tJf "$1";;
  *.zip) unzip -l "$1";;
  *.rar) unrar l "$1";;
  *.7z) 7z l "$1";;
  *) cat "$1";;
esac
