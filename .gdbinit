set history save on
set print pretty on
set pagination off
set confirm off
set breakpoint pending on
set style sources off
add-auto-load-safe-path /home/btolsch/code/whyuslow/.gdbinit
source /home/btolsch/code/zig/tools/zig_gdb_pretty_printers.py
