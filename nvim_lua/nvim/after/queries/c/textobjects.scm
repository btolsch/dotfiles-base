;; extends

((initializer_list
  "," @_start
  .
  (comment)*
  .
  ; NOTE(btolsch): Can't use (_) because the query language is garbage.  Here and below it will end
  ; up being a comment if at all possible, and there's no way to write (^comment).  I also can't
  ; shorten this with an alias or anything.  If there end up being more node types we need, add them
  ; to all the below cases.
  [(number_literal)(binary_expression)(call_expression)(identifier)] @parameter.inner)
  (#make-range! "parameter.outer" @_start @parameter.inner))

((initializer_list
  .
  (comment)+ @_start
  .
  [(number_literal)(binary_expression)(call_expression)(identifier)] @parameter.inner
  .
  ","? @_end)
  (#make-range! "parameter.outer" @_start @_end))

((initializer_list
  .
  [(number_literal)(binary_expression)(call_expression)(identifier)] @parameter.inner
  .
  ","? @_end)
  (#make-range! "parameter.outer" @parameter.inner @_end))
