#!/bin/bash

# NOTE(btolsch): How to use this:
#
# - Create a client on developer.spotify.com.
# - Put the client ID and client secret they assign you into ~/.config/spotify.
# - Run this script and go to the URL it prints in a browser.  If you grant it
#   permission, copy the "code" query string parameter into ~/.config/spotify.
# - Rerun this script and copy the tokens into ~/.config/spotify.
# - If your token expires, delete |ACCESS_TOKEN| from ~/.config/spotify and
#   rerun.  It should just refresh and you can copy a new access token into
#   ~/.config/spotify.
#
# The lemonbar component will query spotify if ~/.spotify exists, but reverts to
# MPD otherwise.

source ~/.config/spotify

if [[ -z $CODE ]]; then
  echo "https://accounts.spotify.com/authorize?client_id=$CLIENT_ID&response_type=code&redirect_uri=http://localhost&scope=user-read-playback-state"
  exit 1
fi

if [[ -z $ACCESS_TOKEN ]]; then
  SECRET=$(echo -n $CLIENT_ID:$CLIENT_SECRET | base64 | tr -d '\r\n')
  if [[ -z $REFRESH_TOKEN ]]; then
    TOKEN_RESULT=$(curl -X "POST" -H "Authorization: Basic $SECRET" -d grant_type=authorization_code -d code=$CODE -d redirect_uri=http://localhost https://accounts.spotify.com/api/token --silent)
    if [[ $(echo "$TOKEN_RESULT" | jq '. | has("error")') == "true" ]]; then
      echo "error getting access token"
      echo "$TOKEN_RESULT"
      exit 1
    else
      ACCESS_TOKEN=$(echo "$TOKEN_RESULT" | jq '.access_token' | tr -d '"')
      REFRESH_TOKEN=$(echo "$TOKEN_RESULT" | jq '.refresh_token' | tr -d '"')
      echo "got token"
      echo "ACCESS_TOKEN=\"$ACCESS_TOKEN\""
      echo "REFRESH_TOKEN=\"$REFRESH_TOKEN\""
    fi
  else
    TOKEN_RESULT=$(curl -X "POST" -H "Authorization: Basic $SECRET" -d grant_type=refresh_token -d refresh_token=$REFRESH_TOKEN https://accounts.spotify.com/api/token --silent)
    if [[ $(echo "$TOKEN_RESULT" | jq '. | has("error")') == "true" ]]; then
      echo "error getting access token"
      echo "$TOKEN_RESULT"
      exit 1
    else
      ACCESS_TOKEN=$(echo "$TOKEN_RESULT" | jq '.access_token' | tr -d '"')
      echo "got token"
      echo "ACCESS_TOKEN=\"$ACCESS_TOKEN\""
    fi
  fi
  exit
else
  echo "tokens present"
  curl -X "GET" "https://api.spotify.com/v1/me/player/currently-playing" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer $ACCESS_TOKEN" --silent
fi
