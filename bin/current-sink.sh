#!/bin/bash
notify-send --urgency=low -t 2000 "$(pacmd list-sinks | sed -n '/\* index/,/device\.description/p' | tail -1 | sed 's/^.*description = "\([^"]\+\)".*$/\1/')"
